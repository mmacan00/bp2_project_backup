#pragma once

#include <vector>
#include <string>

namespace decomposition {
	using namespace std;

	class model {
	private:
		vector<string> relation_schemas, attributes, keys, functions, fo, result;
	public:
		model() {};
		~model() {};
		void add_relation_schema(string s);
		string get_relation_schema(size_t i);
		vector<string> get_relation_schemas();
		void add_attribute(string s);
		string get_attribute(size_t i);
		vector<string> get_attributes();
		void add_key(string s);
		string get_key(size_t i);
		vector<string> get_keys();
		void add_function(string s);
		string get_function(size_t i);
		vector<string> get_functions();
		void add_fo(string s);
		string get_fo(size_t i);
		vector<string> get_fos();
		//size_t fo_size();
		//size_t keys_size();
		void add_to_result(string s);
		string get_part_of_result(size_t i);
		vector<string> get_result();
		//size_t result_size();
		void delete_current();
	};
}