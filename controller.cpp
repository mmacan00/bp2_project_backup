﻿#include "controller.h"

#include <iostream>
#include <sstream>
#include <cstdio>
#include <algorithm>
#include <iomanip>

namespace decomposition {

	void controller::run_app() {
		size_t selection;
		do {
			cout << "Select:" << endl
				<< "0.) Exit" << endl
				<< "1.) New file or open file" << endl;
			cin >> selection;
			system("CLS");
			if(std::cin.fail()) {
				cin.clear();
				cin.ignore();
			}
			else if(selection == 1) {
				action();
			}
		}
		while(selection != 0);
	}

	void controller::action() {
		cout << "Enter file path: ";
		string file_path;
		cin >> file_path;
		system("CLS");
		working_file.open(file_path);
		if(!working_file.is_open()) {
			ofstream new_file;
			new_file.open(file_path);
			if(!new_file.is_open()) {
				cout << "Error creating file" << endl;
			}
			else {
				new_file.close();
				working_file.open(file_path);
			}
		}
		if(working_file.is_open()) {
			fill_relations();
			while(true) {
				cout << "Select:" << endl
					<< "0.) Close file" << endl
					<< "1.) Select relation schema" << endl
					<< "2.) Add relation schema" << endl;
				size_t selection;
				cin >> selection;
				system("CLS");
				if(std::cin.fail()) {
					cin.clear();
					cin.ignore();
				}
				else if(selection == 0) {
					working_file.close();
					m.~model();
					return;
				}
				else if(selection == 1) {
					m.delete_current();
					select_row(file_path);
				}
				else if(selection == 2) {
					m.~model();
					add_new_relation_schema(working_file);
					fill_relations();
				}
			}
		}
	}

	void controller::fill_relations() {
		working_file.seekp(0, ios::beg);
		while(!working_file.eof()) {
			string temp;
			getline(working_file, temp);
			if(temp != "") {
				while(true) {
					size_t pos = temp.find(' ');
					if(pos != string::npos) {
						temp.erase(pos, 1);
					}
					else {
						break;
					}
				}
				m.add_relation_schema(temp);
			}
		}
	}

	void controller::select_row(string file_path) {
		size_t length = m.get_relation_schemas().size();
		size_t selection = 0;
		do {
			cout << "Select number for relation you wish to operate on, or 0 to go back:\n";
			show_relations(m.get_relation_schemas());
			cin >> selection;
			system("CLS");
			if(std::cin.fail()) {
				cin.clear();
				cin.ignore();
			}
			else if (selection <= length) {
				if(selection == 0) {
					return;
				}
				cout << "Selected: " << m.get_relation_schema(selection - 1) << endl;
				size_t operation = select_operation();
				if(operation == 1) {
					try {
						slice_relation_schema(m.get_relation_schema(selection - 1));
					}
					catch(string s) {
						cout << s << endl;
						m.delete_current();
						return;
					}
					catch(...) {
						cout << "Default exception!" << endl;
						m.delete_current();
						return;
					}
					if(check_3nf()) {
						cout << "Relation is already in third normal form.\n";
					}
					else {
						calculate_3nf();
					}
				}
				else if(operation >= 2) {
					string file_path_to_change = delete_relation(selection - 1, operation);
					updating_file_after_change(file_path, file_path_to_change);
				}
			}
		}
		while(selection > length);
	}

	size_t controller::select_operation() {
		size_t selection;
		while(true) {
			cout << "Select:" << endl
				<< "0.) Back" << endl
				<< "1.) Calculate third normal form" << endl
				<< "2.) Delete relation" << endl
				<< "3.) Update relation" << endl;
			cin >> selection;
			system("CLS");
			if(std::cin.fail()) {
				cin.clear();
				cin.ignore();
			}
			else if(selection <= 3) {
				return selection;
			}
		}
	}

	string controller::delete_relation(size_t deleteline, size_t is_update) {
		fstream new_file;
		string new_file_name = "new_file.txt";
		new_file.open(new_file_name, ios::out | ios::trunc);
		if(!new_file.is_open()) {
			cout << "Error opening file" << endl;
		}
		size_t length = m.get_relation_schemas().size();
		for(size_t i = 0; i < length; i++) {
			if(i != deleteline) {
				new_file << m.get_relation_schema(i) << endl;
			}
			else if(is_update == 3) {
				add_new_relation_schema(new_file);
			}
		}
		new_file.close();
		return new_file_name;
	}

	void controller::updating_file_after_change(string orig_file_path, string new_file_path) {
		working_file.close();
		if(remove(orig_file_path.c_str()) != 0) {
			cout << "Error updating file" << endl;
		}
		else {
			if(rename(new_file_path.c_str(), orig_file_path.c_str()) != 0) {
				cout << "Error updating file" << endl;
			}
			else {
				working_file.open(orig_file_path);
				if(working_file.is_open()) {
					remove(new_file_path.c_str());
					m.~model();
					fill_relations();
					cout << "File successfully updated" << endl;
				}
				else {
					cout << "Error updating file" << endl;
				}
			}
		}
	}

	void controller::slice_relation_schema(string s) {
		stringstream ss(s);
		getline(ss, s, '|');
		slice_attribute(s, m);
		getline(ss, s, '|');
		slice_keys(s, m);
		getline(ss, s, '|');
		slice_functions(s, m);
	}

	void controller::slice_attribute(string s, model & m) {
		if(s == "") throw string("Empty attributes");
		stringstream ss(s);
		while(getline(ss, s, ',')) {
			if(check_attribute(s, m)) {
				m.add_attribute(s);
			}
		}
	}

	void controller::slice_keys(string s, model & m) {
		if(s == "") throw string("Key missing!");
		stringstream ss(s);
		while(getline(ss, s, ';')) {
			if(check_key(s, m)) {
				m.add_key(s);
			}
		}
	}

	void controller::slice_functions(string s, model & m) {
		if(s == "") throw string("Functions missing!");
		stringstream ss(s);
		while(getline(ss, s, ';')) {
			if(check_function(s, m)) {
				m.add_function(s);
				size_t pos = s.find("->");
				if(pos != string::npos) {
					m.add_fo(s.substr(0, pos));
					m.add_fo(s.substr(pos + 2));
				}
			}
		}
	}

	bool controller::check_attribute(string s, model & m) {
		vector<string> temp_vec = m.get_attributes();
		vector<string>::iterator it;
		it = find(temp_vec.begin(), temp_vec.end(), s);
		if(it != temp_vec.end()) {
			throw string("Same attribute already entered!");
		}
		return true;
	}

	bool controller::check_key(string s, model & m) {
		stringstream ss(s);
		while(getline(ss, s, ',')) {
			vector<string> temp_vec = m.get_attributes();
			vector<string>::iterator it;
			it = find(temp_vec.begin(), temp_vec.end(), s);
			if(it == temp_vec.end()) {
				throw string("Key not in valid format!");
			}
		}
		return true;
	}

	bool controller::check_function(string s, model & m) {
		size_t pos = s.find("->");
		if(pos != string::npos) {
			string temp1 = s.substr(0, pos);
			string temp2 = s.substr(pos + 2);
			stringstream ss(temp1);
			vector<string> temp_vec = m.get_attributes();
			vector<string>::iterator it;
			while(getline(ss, temp1, ',')) {
				it = find(temp_vec.begin(), temp_vec.end(), temp1);
				if(it == temp_vec.end()) {
					throw string("Function not in valid format!");
				}
			}
			ss.clear();
			ss.str(temp2);
			while(getline(ss, temp2, ',')) {
				it = find(temp_vec.begin(), temp_vec.end(), temp2);
				if(it == temp_vec.end()) {
					throw string("Function not in valid format!");
				}
			}
			return true;
		}
		throw string("Function not in valid format!");
	}

	bool controller::check_3nf() {
		size_t length = m.get_fos().size();
		for(size_t i = 0; i < length; i += 2) {
			bool result = (check_trivial(m.get_fo(i), m.get_fo(i + 1)) || check_superkey(m.get_fo(i)) || check_elementary(m.get_fo(i + 1)));
			if(!result) {
				return result;
			}
		}
		return true;
	}

	bool controller::check_trivial(string x, string y) {
		if((x.find(y)) != string::npos) {
			return true;
		}
		return false;
	}

	bool controller::check_superkey(string x) {
		size_t length = m.get_keys().size();
		for(size_t i = 0; i < length; i++) {
			if((x.find(m.get_key(i))) != string::npos) {
				return true;
			}
		}
		return false;
	}

	bool controller::check_elementary(string y) {
		size_t length = m.get_keys().size();
		for(size_t i = 0; i < length; i++) {
			if((m.get_key(i).find(y)) != string::npos) {
				return true;
			}
		}
		return false;
	}

	void controller::calculate_3nf() {
		size_t length = m.get_fos().size();
		for(size_t i = 0; i < length; i += 2) {
			string temp = m.get_fo(i) + ',' + m.get_fo(i + 1);
			if(!does_exist(temp)) {
				m.add_to_result(temp);
			}
			cout << setfill('.') << setw(5) << left << "For: " << setw(20) << left << temp + ' ' << setw(40) << right << " p := ";
			show_result(m.get_result());
		}
		length = m.get_keys().size();
		for(size_t i = 0; i < length; i++) {
			if(does_exist(m.get_key(i))) {
				cout << setfill('.') << setw(20) << left << "Key already inside: " << setw(39) << left << m.get_key(i) + ' ' << setw(6) << right << " p := ";
				show_result(m.get_result());
				cout << endl;
				return;
			}
		}
		m.add_to_result(m.get_key(0));
		cout << setfill('.') << setw(27) << left << "No keys in result, adding: " << setw(32) << left << m.get_key(0) + ' ' << setw(6) << right << " p := ";
		show_result(m.get_result());
		cout << endl;
	}

	bool controller::does_exist(string s) {
		bool key_flag;
		size_t length = m.get_result().size();
		stringstream ss;
		for(size_t i = 0; i < length; i++) {
			ss.clear();
			ss.str(s);
			key_flag = true;
			string temp;
			while(getline(ss, temp, ',')) {
				if((m.get_part_of_result(i).find(temp)) == string::npos) {
					key_flag = false;
					break;
				}
			}
			if(key_flag) {
				return true;
			}
		}
		return false;
	}

	void controller::add_new_relation_schema(fstream & fp) {
		model checker;
		string rel_schema;
		fp.clear();
		fp.seekg(0, ios::end);
		enter_attributes(rel_schema, checker);
		enter_keys(rel_schema, checker);
		enter_functions(rel_schema, checker);		
		fp << rel_schema << endl;
	}

	void controller::enter_attributes(string & rel_schema, model & m) {
		string args, temp;
		while(true) {
			try {
				cout << "press Enter to add or Ctrl+Z to stop input\nEnter argument:" << endl;
				args.clear();
				cin.clear();
				while(cin >> temp) {
					args += temp + ',';
				}
				args.pop_back();
				slice_attribute(args, m);
				rel_schema += args + '|';
				break;
			}
			catch(string s) {
				cout << s << endl;
			}
		}
	}

	void controller::enter_keys(string & rel_schema, model & m) {
		string args, temp;
		while(true) {
			try {
				cout << "press Enter to add or Ctrl+Z to stop input\nEnter key (separate with comma attributes):" << endl;
				args.clear();
				cin.clear();
				while(cin >> temp) {
					args += temp + ';';
				}
				args.pop_back();
				slice_keys(args, m);
				rel_schema += args + '|';
				break;
			}
			catch(string s) {
				cout << s << endl;
			}
		}
	}

	void controller::enter_functions(string & rel_schema, model & m) {
		string args, temp;
		while(true) {
			try {
				cout << "press Enter to add or Ctrl+Z to stop input\nEnter function in format X->A (separate with comma attributes):" << endl;
				args.clear();
				cin.clear();
				while(cin >> temp) {
					args += temp + ';';
				}
				args.pop_back();
				slice_functions(args, m);
				rel_schema += args;
				break;
			}
			catch(string s) {
				cout << s << endl;
			}
		}
	}

	void controller::show_relations(vector<string> r) {
		size_t length = r.size();
		for(size_t i = 0; i < length; i++) {
			cout << i + 1 << ".) " << r[i] << endl;
		}
	}

	void controller::show_result(vector<string> result) {
		size_t length = result.size();
		cout << '{';
		for(size_t i = 0; i < length; i++) {
			cout << '(' << result[i] << ')';
		}
		cout << '}' << endl;
	}

}