#include "controller.h"
#include <iostream>
#include <sstream>

int main() {
	using namespace decomposition;

	app* myApp = new controller;
	myApp->run_app();

	delete myApp;

	return 0;
}