#pragma once

#include "model.h"

#include <fstream>

namespace decomposition {
	using namespace std;

	class app {
	public:
		virtual void run_app() = 0;
		virtual ~app() {};
	};

	class controller : public app {
		model m;
		fstream working_file;
	public:
		controller() {};
		~controller() {};
		void run_app();
		void action();
		void fill_relations();
		void select_row(string file_path);
		size_t select_operation();
		string delete_relation(size_t deleteline, size_t is_update);
		void updating_file_after_change(string orig_file_path, string new_file_path);
		void slice_relation_schema(string s);
		void slice_attribute(string s, model & m);
		void slice_keys(string s, model & m);
		void slice_functions(string s, model & m);
		bool check_attribute(string s, model & m);
		bool check_key(string s, model & m);
		bool check_function(string s, model & m);
		bool check_3nf();
		bool check_trivial(string x, string y);
		bool check_superkey(string x);
		bool check_elementary(string y);
		void calculate_3nf();
		bool does_exist(string s);
		void add_new_relation_schema(fstream & fp);
		void enter_attributes(string & rel_schema, model & m);
		void enter_keys(string & rel_schema, model & m);
		void enter_functions(string & rel_schema, model & m);
		void show_relations(vector<string> r);
		void show_result(vector<string> result);
	};	
}
