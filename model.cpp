#include "model.h"

namespace decomposition {

	void model::add_relation_schema(string s) {
		relation_schemas.push_back(s);
	}

	string model::get_relation_schema(size_t i) {
		return relation_schemas[i];
	}

	vector<string> model::get_relation_schemas() {
		return relation_schemas;
	}

	void model::add_attribute(string s) {
		attributes.push_back(s);
	}

	string model::get_attribute(size_t i) {
		return attributes[i];
	}

	vector<string> model::get_attributes() {
		return attributes;
	}

	void model::add_key(string s) {
		keys.push_back(s);
	}

	string model::get_key(size_t i) {
		return keys[i];
	}

	vector<string> model::get_keys() {
		return keys;
	}

	void model::add_function(string s) {
		functions.push_back(s);
	}

	string model::get_function(size_t i) {
		return functions[i];
	}

	vector<string> model::get_functions() {
		return functions;
	}

	void model::add_fo(string s) {
		fo.push_back(s);
	}

	string model::get_fo(size_t i) {
		return fo[i];
	}

	vector<string> model::get_fos() {
		return fo;
	}

	/*size_t model::fo_size() {
		return fo.size();
	}

	size_t model::keys_size() {
		return keys.size();
	}*/

	void model::add_to_result(string s) {
		result.push_back(s);
	}

	string model::get_part_of_result(size_t i) {
		return result[i];
	}

	vector<string> model::get_result() {
		return result;
	}

	/*size_t model::result_size() {
		return result.size();
	}*/

	void model::delete_current() {
		attributes.clear();
		keys.clear();
		functions.clear();
		fo.clear();
		result.clear();
	}

}